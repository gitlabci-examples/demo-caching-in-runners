# demo-caching-in-runners

This is an example project to demonstrate how to caching in GitLab CI/CD.

For further information, please refer to <https://docs.gitlab.com/ee/ci/caching/>
