import numpy as np

n = 32

A = np.random.rand(n,n)
B = np.linalg.inv(A)
C = A @ B

assert(np.allclose(C.diagonal(), np.ones(n)))
